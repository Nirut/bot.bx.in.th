#include <IE.au3>

;Bot BX.in.th

$oIE = _IECreate("https://bx.in.th/THB/XRP/", 0, 0, 1, 0 )

Func getDataTrade()

	$spans = _IETagNameGetCollection($oIE, "span")

	For $span In $spans
		If ( $span.className == "last" ) Then
			ConsoleWrite( "Latest : " & $span.innerText & @CRLF )
		ElseIf ( $span.className == "change" ) Then
			;ConsoleWrite( "Change : " & $span.innerText & @CRLF )
		ElseIf ( $span.className == "high" ) Then
			;ConsoleWrite( "High : " & $span.innerText & @CRLF )
		ElseIf ( $span.className == "low" ) Then
			;ConsoleWrite( "Low : " & $span.innerText & @CRLF )
		ElseIf ( $span.className == "volume" ) Then
			;ConsoleWrite( "Volume : " & $span.innerText & @CRLF )
		ElseIf ( $span.className == "avg" ) Then
			;ConsoleWrite( "Avg : " & $span.innerText & @CRLF )
		EndIf
	Next

EndFunc


Local $i = 0
Do
	getDataTrade()
	$i = $i + 1
	Sleep(600000)
Until $i = -1 ;loop
ConsoleWrite( "Avg : " & @CRLF )
_IEQuit($oIE)

;avg