; --------------------------
; Bot BX.in.th
; --------------------------

#include <IE.au3>
#include <MsgBoxConstants.au3>
#include <StringConstants.au3>

$balance_buy = 0
$balance_sell = 0

; var trading
$trading = "XRP"
$val_change = "0%"
$val_buy = 0
$val_last = 0
$val_high = 0
$val_low = 0
$val_avg = 0
$val_volume = 0

;url trade
$oIE = _IECreate("https://bx.in.th/THB/XRP/")

; check balance buy and sell
Func getBalance()
	Local $i = 0
	For $div In $divs
		If ($div.className == "control-group available") Then

			$i = $i + 1
			$val = StringRegExpReplace(Number($div.innerText), "[^0-9]", "")

			If ( $i == 1 ) Then
				$balance_buy = $val
			ElseIf ( $i == 2 ) Then
				$balance_sell = $val
			EndIf
		EndIf
	Next

	#comments-start
	; check auto buy, sell
	If ( $balance_buy == 0 ) And ( $balance_sell == 0 ) Then
		ConsoleWrite( "[ Can't Buy and Can't Sell ]" & @CRLF )
	ElseIf ( $balance_sell == 0 ) Then
		ConsoleWrite( $trading & " to sell : " & $balance_sell & "[ Checking to Sell" & $trading & " ]" & @CRLF )
	Else
		ConsoleWrite( $trading & " Available Balance : " & $balance_buy & "[ Checking to Buy" & $trading & " ]" & @CRLF )
	EndIf
	#comments-end
EndFunc

Func calPercent( $num1, $num2 )
	$sum = ($num1 / $num2 * 100) - 100
	Return StringFormat("%.2f", $sum)
EndFunc

Func getDataTrade()

	Local $status = ""

	$divs = _IETagNameGetCollection($oIE, "div")
	$spans = _IETagNameGetCollection($oIE, "span")

	For $span In $spans
		If ($span.className == "last") Then

			; check up down
			If ( $val_last == 0 ) Then
				$status = "Start"
			ElseIf ( Number($span.innerText) == $val_last ) Then
				$status = "Not Change"
			ElseIf ( Number($span.innerText) > $val_last ) Then
				$status = "Up"
			ElseIf ( Number($span.innerText) < $val_last ) Then
				$status = "Down"
			EndIf

			$val_last = Number($span.innerText)

		ElseIf ($span.className == "change") Then
			$val_change = $span.innerText
		ElseIf ($span.className == "high") Then
			$val_high = Number($span.innerText)
		ElseIf ($span.className == "low") Then
			$val_low = Number($span.innerText)
		ElseIf ($span.className == "volume") Then
			$val_volume = $span.innerText
		ElseIf ($span.className == "avg") Then
			$val_avg = Number($span.innerText)
		EndIf
	Next

	If Not ( $status == "Not Change" ) Then
		ConsoleWrite( "Lastest : " & $val_last & " " & $status & @CRLF )
	EndIf

	;ConsoleWrite( "Latest : " & $val_last & " / Change : " & $val_change & " / High : " & $val_high & " / Low : " & $val_low & " / 24 Hours Vol : " & $val_volume & " / Avg : " & $val_avg & @CRLF )

	; $profit_thb = String( ( $val_last * $balance_sell ) - ( $val_buy * $balance_sell ) ) & " THB (" & calPercent( $val_last, $val_buy ) & "%)"
	; ConsoleWrite( "Buy : " & $val_buy & " / Latest : " & $val_last & " / Profit : " & $profit_thb & @CRLF )
EndFunc

Func runtimeCheckTrade()
	Local $i = 0
	Do
		getDataTrade()
		$i = $i + 1
		Sleep(8000)
	Until $i = -1
EndFunc

; --------------------------
; Start Function
; --------------------------
; getBalance()
runtimeCheckTrade()

_IEQuit($oIE)
